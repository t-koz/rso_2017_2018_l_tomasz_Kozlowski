﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceLibrary1
{
    public class Book
    {
        int bookID { get; }
        string author { get; }
        string tytul;
        int osobaID;
        DateTime returnDate = new DateTime();
        DateTime borrowDate = new DateTime();
        public Book(int bookID, int osobaID, string author, string tytul, DateTime returnDate, DateTime borrowDate)
        {
            this.bookID = bookID;
            this.osobaID = osobaID;
            this.author = author;
            this.tytul = tytul;
            this.returnDate = returnDate;
            this.borrowDate = borrowDate;
        }

        public int getBookID()
        {
            return bookID;
        }
        public string getAuthor()
        {
            return author;
        }
        public DateTime getReturnDate()
        {
            return returnDate;
        }
        public DateTime getBorrowDate()
        {
            return borrowDate;
        }
        public string getTitle()
        {
            return tytul;
        }
        public int getOsobaID()
        {
            return osobaID;
        }
        public void setOsobaID(int id)
        {
            osobaID = id;
        }
        public void setBorrowTime()
        {
            borrowDate = DateTime.Now;
        }
        public void setReturnDate()
        {
            returnDate = borrowDate.AddDays(14);
        }


    }
}
