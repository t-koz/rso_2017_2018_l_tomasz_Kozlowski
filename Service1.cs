﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary1
{
    // UWAGA: możesz użyć polecenia „Zmień nazwę” w menu „Refaktoryzuj”, aby zmienić nazwę klasy „Service1” w kodzie i pliku konfiguracji.
    public class Service1 : IService1
    {
        ListOfBooks listaKsiazek = new ListOfBooks();
        public Hashtable wypozyczoneKsiazki = new Hashtable();
        public string GetData(string value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
        public List<Book> getList()
        {
            return listaKsiazek.getList();
        }

        public string getAllBooksList()
        {

            string toReturn = "";
            foreach (Book i in ListOfBooks.allBooksList)
            {
                toReturn += i.getBookID() + "#" + i.getAuthor() + "#" + i.getTitle() + "#";
            }
            return toReturn;
        }

        public List<Object> listOfBorrowedItems()
        {
            List<Object> listaWypozyczonychKsiazek = new List<Object>();
            foreach (Book i in ListOfBooks.allBooksList)
            {
                if((i.getOsobaID()) != 0)
                {
                    listaWypozyczonychKsiazek.Add(i);
                }
            }
            return listaWypozyczonychKsiazek;
        }
        public string getBorrowedBooks(int userID)
        {
            string toReturn = "";
            foreach (Book i in ListOfBooks.allBooksList)
            {
                if ((i.getOsobaID()) == userID)
                {
                    toReturn += i.getBookID() + "#" + i.getAuthor() + "#" + i.getTitle() + "#";
                }
            }
            return toReturn;
        }
        public string getBookInfo(int bookID)
        {
            string toReturn = "";
            foreach (Book i in ListOfBooks.allBooksList)
            {
                if ((i.getBookID()) == bookID)
                {
                    toReturn += i.getBookID() + "#" + i.getAuthor() + "#" + i.getTitle() + "#" + i.getBorrowDate() + "#" + i.getReturnDate();
                }
            }
            return toReturn;
        }
        public bool borrowBook(int osobaID, int bookID)
        {
            if (bookID <= 0)
            {
                Exception exception = new Exception();
                throw new FaultException<Exception>(exception, "nie ma takiej ksiązki");
            }

            bool znalezionoKsiazke = false;
            if (osobaID <= 0 || osobaID > 10)
            {
                FormatException form = new FormatException();
                throw new FaultException<FormatException>(form, "Niepoprawnie wprowadzone dane");
            }
            foreach (Book i in ListOfBooks.allBooksList)
            {
                if (i.getBookID() == bookID)
                {
                    znalezionoKsiazke = true;
                    if (i.getOsobaID() == osobaID)
                    {
                        Exception trzy = new Exception();
                        throw new FaultException<Exception>(trzy, "Już wypożyczyłeś tę książkę");
                    }
                    if (i.getOsobaID() != 0)
                    {
                        Exception dwa = new Exception();
                        throw new FaultException<Exception>(dwa, "Ktoś inny wypożyczył tę książkę");
                    }
                    i.setOsobaID(osobaID);
                    i.setBorrowTime();
                    i.setReturnDate();
                    znalezionoKsiazke = true;
                }
                
            }
            return znalezionoKsiazke;
        }

        public string[] array()
        {
            List<Object> listaWypozyczonychKsiazek = new List<Object>();
            int tabLen = 0;
            object[] a = new object[10];
            int licznik = 0;
            foreach (Book i in ListOfBooks.allBooksList)
            {
                if ((i.getOsobaID()) != 0)
                {
                    a[licznik] = i.getBookID() + "#" + i.getAuthor() + "#" + i.getTitle() + "#" + i.getBorrowDate() + "#" + i.getReturnDate(); ;
                    tabLen++;
                    licznik++;
                }
            }

            string[] listaa = new string[tabLen];
            for (int i = 0; i < tabLen; i++)
            {
                listaa[i] = a[i].ToString();
            }

            return listaa;
        }
    }
}

