﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceLibrary1
{
    
    // UWAGA: możesz użyć polecenia „Zmień nazwę” w menu „Refaktoryzuj”, aby zmienić nazwę interfejsu „IService1” w kodzie i pliku konfiguracji.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string getAllBooksList();
        [OperationContract]
        string GetData(string value);
        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);
        [OperationContract]
        List<Object> listOfBorrowedItems();
        //[OperationContract]
        //List<Book> getList();
        [OperationContract]
        string [] array();
        [OperationContract]
        string getBorrowedBooks(int userID);
        [OperationContract]
        string getBookInfo(int bookID);
        [OperationContract]
        [FaultContract(typeof(Exception))]
        [FaultContract(typeof(FormatException))]
        bool borrowBook(int osobaID, int bookID);

        // TODO: dodaj tutaj operacje usługi
    }

    // Użyj kontraktu danych, jak pokazano w poniższym przykładzie, aby dodać typy złożone do operacji usługi.
    // Możesz dodać pliki XSD do projektu. Po skompilowaniu projektu możesz bezpośrednio użyć zdefiniowanych w nim typów danych w przestrzeni nazw „WcfServiceLibrary1.ContractType”.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    //[DataContract]
    //public class ListofBooks
    //{
    //    [DataMember]
    //    List<Book> getLista { get;  }
    //}

}
