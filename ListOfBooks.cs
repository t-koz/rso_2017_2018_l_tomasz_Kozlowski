﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceLibrary1
{
    class ListOfBooks
    {
        DateTime data = new DateTime();
        public static ArrayList allBooksList = new ArrayList {
            new Book(1, 0, "R R Martin", "Gra o tron", new DateTime(), new DateTime()),
            new Book(2, 1, "Julian Tuwim", "Lokomotywa", new DateTime(), new DateTime()),
            new Book(3, 0, "Julian Tuwim", "Słoń Trąbalski",new DateTime(), new DateTime()),
            new Book(4, 0, "Julian Tuwim", "Kwiaty polskie",new DateTime(), new DateTime()),
            new Book(5, 6, "Bolesław Leśmian", "Łąka", new DateTime(), new DateTime()),
            new Book(6, 0, "Juliusz Słowacki", "Rozmowa z piramidami", new DateTime(), new DateTime()),
            new Book(7, 0, "Juliusz Słowacki", "Oda do wolności ", new DateTime(), new DateTime()),
            new Book(8, 2, "Jan Brzechwa", "Talizmany", new DateTime(), new DateTime()),
            new Book(9, 0, "Jan Brzechwa", "Teatr Pietruszki", new DateTime(), new DateTime()),
            new Book(10, 0, "Jan Brzechwa", "Przygody Pchły Szachrajki", new DateTime(), new DateTime()),
    };

        public List<Book> getList()
        {
            List<Book> listaWypozyczonychKsiazek = new List<Book>();
            foreach (Book i in ListOfBooks.allBooksList)
            {
                if ((i.getOsobaID()) != 0)
                {
                    listaWypozyczonychKsiazek.Add(i);
                }
            }
            return listaWypozyczonychKsiazek;
        }
    }
}
